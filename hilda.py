# encoding: utf-8
"""
Chief Judge Hilda Margaret McGruder herself. Reads e-mails and process their content.

Hilda est un juge de jeu qui fonctionne par étape :

1. Récupérer l'ensemble des emails et les stocker sur le disque
2. Pour chaque email sur le disque
    2.1. le charger
    2.2. le traiter
    2.3. sauver les réponses éventuelles
    2.4. supprimer le fichier
3. Envoyer l'ensemble des réponses présentes, puis les supprimer
"""

from importlib import import_module
import os

from dotenv import load_dotenv
from envelopes import Envelope
from envelopes.conn import SMTP
from imbox import Imbox

from message import Message
from logger import Logger


load_dotenv()
recipient_filter = os.getenv("RECIPIENT_FILTER", None)

# Create structure if necessary
INBOX_PATH = os.getenv("INBOX_PATH", "data/inbox")
os.makedirs(INBOX_PATH, exist_ok=True)
OUTBOX_PATH = os.getenv("OUTBOX_PATH", "data/outbox")
os.makedirs(OUTBOX_PATH, exist_ok=True)
APPS_PATH = os.getenv("APPS_PATH", "data/apps")
os.makedirs(APPS_PATH, exist_ok=True)
LOGS_PATH = os.getenv("LOGS_PATH", "logs")
os.makedirs(LOGS_PATH, exist_ok=True)


def get_messages():
    total = 0
    counter = 0
    with Imbox(
        os.getenv("IMAP_SERVER", "localhost"),
        username=os.getenv("IMAP_USER", ""),
        password=os.getenv("IMAP_PASSWORD", ""),
        ssl=False,
        ssl_context=None,
        starttls=False,
    ) as imbox:
        for uid, email in imbox.messages():
            total += 1
            message = Message().from_email(email)
            if recipient_filter is None or recipient_filter in message.recipient:
                counter += 1
                message.save(os.path.join(INBOX_PATH, f"{int(uid)}"))
            # Delete email on mail server
            imbox.delete(uid)
    logger.info(f"{counter} message(s) saved on a total of {total}.")

def process_messages():
    for email in os.listdir(INBOX_PATH):
        message = Message().load(os.path.join(INBOX_PATH, email))
        app_name = message.recipient.split("@")[0]
        try:
            module = import_module(f"apps.{app_name}.{app_name}")
            app = getattr(module, "App")()
            app.process(message)
        except Exception as e:
            logger.error(e)
            logger.error(f"Application '{app_name}' unknown.")
        # Delete email once processed
        os.remove(os.path.join(INBOX_PATH, email))

def send_messages():
    total = 0
    counter = 0
    smtp = SMTP(
        os.getenv("SMTP_SERVER", "localhost"),
        login=os.getenv("SMTP_USER", ""),
        password=os.getenv("SMTP_PASSWORD", ""),
        tls=True,
    )
    # Get emails from filesystem and send each of them
    for answer in os.listdir(OUTBOX_PATH):
        total += 1
        message = Message().load(os.path.join(OUTBOX_PATH, answer))
        try:
            smtp.send(
                Envelope(
                    from_addr=(message.sender, message.sender_name),
                    to_addr=message.recipient,
                    subject=message.subject,
                    text_body=message.body_text,
                    html_body=message.body_html,
                )
            )
            os.remove(os.path.join(OUTBOX_PATH, answer))
            counter += 1
        except Exception as e:
            logger.error(e)
    logger.info(f"{counter} message(s) sent on a total of {total}.")


if __name__ == "__main__":
    logger = Logger("hilda", LOGS_PATH)
    get_messages()
    process_messages()
    send_messages()

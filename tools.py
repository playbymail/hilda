# encode: utf-8
"""
Different tools to help.
"""

import os
from random import choice


def random_id(size):
    ALPHABET = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ"
    return "".join([choice(ALPHABET) for i in range(size)])


def pick_id(size, path=None):
    next_id = random_id(size)
    if path is not None:
        while os.path.exists(os.path.join(path, next_id)):
            next_id = random_id(size)
    return next_id

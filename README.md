# Fonctionnement

Hilda est un juge de jeu qui fonctionne par étape :

1. Récupérer l'ensemble des emails et les stocker sur le disque
2. Pour chaque email sur le disque
    2.1. le charger
    2.2. le traiter
    2.3. sauver les réponses éventuelles
    2.4. supprimer le fichier
3. Envoyer l'ensemble des réponses présentes, puis les supprimer

Pour jouer, il est donc nécessaire de posséder une adresse mail et c'est tout. C'est par
le biais d'échanges d'emails que l'ensemble des actions sont effectuées.

Hilda fonctionne sur une machine qui est allumée automatiquement une fois par heure. Une
fois son fonctionnement terminé, la machine est alors éteinte. Les échanges d'emails ne
seront donc pris en compte qu'une fois toutes les heures.

## Adresses mails

Les adresses mails sont simplement les alias d'une adresse spécifique. Chaque alias
correspond à une application spécifique. Certains alias ne sont pas des jeux :

* contact
* abuse
* bug

### contact

Cette adresse est redirigée vers une adresse pour traitement humain. Cette adresse sert
à traiter tout type de sujet.

### abuse

Cette adresse est également redirigée vers une adresse pour traitement humain mais
concerne strictement les abus potentiels sur l'un des jeux.

### bug

Cette adresse est utilisée afin de traiter des bugs et soucis ayant trait à l'ensemble
des applications et du site. Chaque bug se voit automatiquement donner un identifiant
qu'il faudra rappeler par la suite pour le traitement de celui-ci.

C'est, bien entendu, un humain qui se charge de répondre, voire de résoudre les bugs,
mais le message du bug est automatiquement lié au bug identifié et ne fait qu'ajouter
une note au bug déjà existant (ou initialiser le bug sinon).

## Les jeux

Chaque jeu a ses propres règles et contraintes (inscription, paramètres, etc...). En
général, chaque jeu est capable de répondre à une commande AIDE.

### Les commandes

La commande AIDE est une commande commune à tous les jeux. Celle-ci permet de comprendre
le fonctionnement du jeu proprement dit et liste les commandes utilisables du jeu. De
même, sera précisé s'il est nécessaire de s'inscrire au jeu, si celui-ci à une fin, si
on peut paramétrer certaines options comme le délai maximal de réponse, etc...

La commande REGLES est également une commande commune à tous les jeux. Elle permet
d'obtenir les règles du jeu.

Dans tous les cas, les commandes d'un même mail sont liées à une seule partie d'un seul
jeu. Et il est souvent nécessaire de signaler le numéro de la partie en première
commande.

## Lancer les tests

Afin de lancer les tests, il faut installer pytest et utiliser le fichier
d'environnement de test :

    pytest --envfile=.test.env

Avec la couverture de code (pytest-cov à installer en surplus) :

    pytest --envfile=.test.env --cov --cov-report=html

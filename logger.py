# encoding: utf-8
"""
Logger for Hilda.
"""

import logging
from logging.handlers import TimedRotatingFileHandler
import os


class Logger:
    def __init__(self, name, path, level=logging.INFO):
        filename = os.path.join(path, f"{name}.log")
        handler = TimedRotatingFileHandler(filename, when="d", backupCount=10)
        handler.setLevel(level)
        handler.setFormatter(logging.Formatter("%(asctime)s %(levelname)s %(message)s"))
        self.logger = logging.getLogger(name)
        self.logger.setLevel(level)
        self.logger.propagate = False
        if not self.logger.hasHandlers():
            self.logger.addHandler(handler)

    def debug(self, *args, **kwargs):
        self.logger.debug(*args, **kwargs)

    def info(self, *args, **kwargs):
        self.logger.info(*args, **kwargs)

    def warning(self, *args, **kwargs):
        self.logger.warning(*args, **kwargs)

    def error(self, *args, **kwargs):
        self.logger.error(*args, **kwargs)

    def critical(self, *args, **kwargs):
        self.logger.critical(*args, **kwargs)

# encoding: utf-8
"""
Application skeleton.
"""

import json
import os
import re

from hilda import APPS_PATH, LOGS_PATH, OUTBOX_PATH
from logger import Logger
from message import Message
import tools


class Application:
    def __init__(self, name, path, sender, sender_name):
        self.commands = {
            "help": r"^HELP(\s+(?P<subject>[^\s]+))?$",
            "rules": r"^RULES?$",
        }
        self.name = name
        self.logger = Logger(self.name, LOGS_PATH)
        self.app_path = os.path.join(APPS_PATH, self.name)
        os.makedirs(self.app_path, exist_ok=True)
        self.template_path = os.path.join(path, "templates")
        self.sender = sender
        self.sender_name = sender_name
        self.data = {"app": self}
        self.message = None

    def parse(self, line):
        for command in self.commands:
            result = re.search(self.commands[command], line, re.I)
            if result is not None:
                return command, result.groupdict()

    def process(self, message):
        self.message = message
        if self.message.subject is not None:
            self.process_subject()
        for line in message.body_text.splitlines():
            if line.strip() != "":
                try:
                    instruction, parameters = self.parse(line.strip())
                    getattr(self, instruction)(**parameters)
                except TypeError:
                    # No command found on this line
                    # If user is messaging
                    # print(f"error on line => {line.strip()}")
                    continue

    def process_subject(self):
        # Abstract method
        pass

    def send_message(self, recipient, template):
        message = Message()
        message.sender = self.sender
        message.sender_name = self.sender_name
        message.recipient = recipient
        message.render(
            template_path=self.template_path,
            template=template,
            data=self.data,
        )
        message.save(os.path.join(OUTBOX_PATH, f"{self.name}_{tools.pick_id(8)}"))

    def save(self):
        with open(os.path.join(self.app_path, self.game_id), "w") as file_handle:
            json.dump(self.serialize(), file_handle)

    def load(self, game_id):
        try:
            with open(os.path.join(self.app_path, game_id), "r") as file_handle:
                self.unserialize(json.load(file_handle))
        except:
            self.logger.warning(f"game {game_id} does not exist")

    def delete(self, game_id):
        try:
            os.remove(os.path.join(self.app_path, game_id))
        except:
            self.logger.warning(f"game {game_id} does not exist")

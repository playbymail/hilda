# encoding: utf-8

import os
import shutil

# Clean old data tests
try:
    shutil.rmtree(os.getenv("DATA_PATH"))
except FileNotFoundError:
    pass

# Create necessary structure
os.makedirs(os.getenv("INBOX_PATH"), exist_ok=True)
os.makedirs(os.getenv("OUTBOX_PATH"), exist_ok=True)
os.makedirs(os.getenv("APPS_PATH"), exist_ok=True)
os.makedirs(os.getenv("LOGS_PATH"), exist_ok=True)

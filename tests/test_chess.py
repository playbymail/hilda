# encoding: utf-8

import os
import pytest

from apps.chess.chess import App
from message import Message
from tests.utils import mail_address


def test_creating_a_new_game(mail_address):
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    assert "foo@bar.com" in game.players
    assert message.sender in game.players
    assert len(game.players) == 2
    assert game.board.turn
    assert len(game.moves) == 0

def test_playing_real_move(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Playing
    message = Message()
    message.sender = game.players[game.board.turn]
    message.subject = f"GAME {game_id}"
    message.body_text = "PLAY e4"
    game = App()
    game.process(message)
    assert not game.board.turn
    assert len(game.moves) == 1

def test_playing_with_bad_email(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Playing
    message = Message()
    message.sender = mail_address()
    message.subject = f"GAME {game_id}"
    message.body_text = "PLAY e4"
    game = App()
    game.process(message)
    assert game.board.turn
    assert len(game.moves) == 0
    assert "Vous ne faites pas partie de ce jeu." in game.error

def test_playing_while_not_my_turn(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Playing
    message = Message()
    message.sender = game.players[not game.board.turn]
    message.subject = f"GAME {game_id}"
    message.body_text = "PLAY e4"
    game = App()
    game.process(message)
    assert game.board.turn
    assert len(game.moves) == 0
    assert "Ce n'est pas à votre tour de jouer." in game.error

def test_playing_illegal_move(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Playing
    message = Message()
    message.sender = game.players[game.board.turn]
    message.subject = f"GAME {game_id}"
    message.body_text = "PLAY h8"
    game = App()
    game.process(message)
    assert game.board.turn
    assert len(game.moves) == 0
    assert "Votre coup semble invalide." in game.error

def test_playing_without_game_id(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Playing
    message = Message()
    message.sender = game.players[game.board.turn]
    message.body_text = "PLAY e4"
    game = App()
    game.process(message)
    assert game.board is None
    assert len(game.moves) == 0
    assert "Il semble qu'aucune partie n'ait été chargée." in game.error

def test_playing_with_bad_game_id(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Playing
    message = Message()
    message.sender = game.players[game.board.turn]
    message.subject = f"GAME AAAAA"
    message.body_text = "PLAY e4"
    game = App()
    game.process(message)
    assert game.board is None
    assert len(game.moves) == 0
    assert "Il semble qu'aucune partie n'ait été chargée." in game.error

def test_asking_help(mail_address):
    message = Message()
    message.sender = mail_address()
    message.body_text = "HELP"
    game = App()
    game.process(message)
    assert game.board is None
    assert len(game.players) == 0
    assert len(game.moves) == 0
    assert game.error is None

def test_asking_rules_only(mail_address):
    message = Message()
    message.sender = mail_address()
    message.body_text = "RULES"
    game = App()
    game.process(message)
    assert game.board is not None
    assert len(game.players) == 0
    assert len(game.moves) == 0
    assert game.error is None

def test_asking_rules_in_the_middle_of_a_game(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Playing
    message = Message()
    message.sender = players[game.board.turn]
    message.subject = f"GAME {game_id}"
    message.body_text = "RULES\nPLAY e4"
    game = App()
    game.process(message)
    assert game.board is not None
    assert len(game.players) == 2
    assert len(game.moves) > 0
    assert game.error is None

def test_checkmate(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Modifying board
    game.board.set_fen("6k1/1R6/R7/8/8/8/8/3K4 w - - 0 1")
    game.save()
    # Playing
    message = Message()
    message.sender = players[game.board.turn]
    message.subject = f"GAME {game_id}"
    message.body_text = "RULES\nPLAY a6a8"
    game = App()
    game.process(message)
    assert game.board.is_checkmate()
    assert game.error is None

def test_game_is_deleted_after_checkmate(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Modifying board
    game.board.set_fen("6k1/1R6/R7/8/8/8/8/3K4 w - - 0 1")
    game.save()
    # Playing
    message = Message()
    message.sender = players[game.board.turn]
    message.subject = f"GAME {game_id}"
    message.body_text = "RULES\nPLAY a6a8"
    game = App()
    game.process(message)
    game = App()
    game.load(game_id)
    assert game.board is None
    assert len(game.players) == 0
    assert len(game.moves) == 0

def test_resigning_game(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Resigning
    message = Message()
    message.sender = players[game.board.turn]
    message.subject = f"GAME {game_id}"
    message.body_text = "RESIGN"
    game = App()
    game.process(message)
    game = App()
    game.load(game_id)
    assert game.board is None
    assert len(game.players) == 0
    assert len(game.moves) == 0

def test_resigning_unknown_game(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Resigning
    message = Message()
    message.sender = players[game.board.turn]
    message.subject = "GAME AAAAA"
    message.body_text = "RESIGN"
    game = App()
    game.process(message)
    assert game.board is None
    assert len(game.moves) == 0
    assert "Il semble qu'aucune partie n'ait été chargée." in game.error

def test_check(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Modifying board
    game.board.set_fen("4k3/8/8/8/8/5R2/7R/4K3 w - - 0 1")
    game.save()
    # Playing
    message = Message()
    message.sender = players[game.board.turn]
    message.subject = f"GAME {game_id}"
    message.body_text = "RULES\nPLAY h2h8"
    game = App()
    game.process(message)
    assert game.board.is_check()
    assert game.error is None

def test_insufficient_material(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Modifying board
    game.board.set_fen("4kR2/8/8/8/8/8/8/4K3 b - - 0 1")
    game.save()
    # Playing
    message = Message()
    message.sender = players[game.board.turn]
    message.subject = f"GAME {game_id}"
    message.body_text = "RULES\nPLAY e8f8"
    game = App()
    game.process(message)
    assert game.board.is_insufficient_material()
    assert game.error is None

def test_stalemate(mail_address):
    # Creating a new game
    message = Message()
    message.sender = mail_address()
    message.body_text = "NEWGAME foo@bar.com"
    game = App()
    game.process(message)
    players = game.players
    game_id = game.game_id
    # Modifying board
    game.board.set_fen("k7/8/2K5/8/8/8/1Q6/8 w - - 0 1")
    game.save()
    # Playing
    message = Message()
    message.sender = players[game.board.turn]
    message.subject = f"GAME {game_id}"
    message.body_text = "RULES\nPLAY b2b6"
    game = App()
    game.process(message)
    assert game.board.is_stalemate()
    assert game.error is None

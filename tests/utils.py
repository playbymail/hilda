# encoding: utf-8

from string import ascii_lowercase
from random import choice, randint

import pytest


@pytest.fixture
def mail_address():
    def _mail_address():
        return f"{''.join([choice(ascii_lowercase) for i in range(randint(3,8))])}@bar.org"

    return _mail_address

# encoding: utf-8
"""
Message is an object used to store and generate emails.
"""

from datetime import datetime
import json
import os

from jinja2 import Environment, FileSystemLoader
from markdown import markdown


class Message:
    def __init__(self):
        self.subject = None
        self.body_text = ""
        self.body_html = ""
        self.sender = None
        self.sender_name = None
        self.recipient = None
        self.recipient_name = None
        self.date = None

    def from_email(self, email):
        """
        Convert email from Imbox to Message.
        """
        # Retrieving charset
        charset = "utf-8"
        for header in email.headers:
            if header["Name"] == "Content-Type":
                for value in header["Value"].split(";"):
                    if "charset" in value:
                        charset = value.split("charset=")[-1].lower()
                        if '"' in charset:
                            charset = charset[1:-1]
        # Subject is not always present
        try:
            self.subject = email.subject
        except AttributeError:
            self.subject = ""
        self.body_text = "".join([line for line in email.body["plain"]])
        if len(email.sent_from) > 0:
            self.sender = email.sent_from[0]["email"]
            self.sender_name = email.sent_from[0]["name"]
        else:
            self.sender = ""
            self.sender_name = ""
        # Defaulting to "To" field, but maybe there are only "CC" recipients
        if len(email.sent_to) > 0:
            self.recipient = email.sent_to[0]["email"]
            self.recipient_name = email.sent_to[0]["name"]
        elif len(email.cc) > 0:
            self.recipient = email.cc[0]["email"]
            self.recipient_name = email.cc[0]["name"]
        else:
            self.recipient = ""
            self.recipient_name = ""
        self.date = datetime.strftime(email.parsed_date, "%Y-%m-%d %H:%M:%S")
        return self

    def render(self, template_path, template, data):
        """
        Render subject and body using jinja2 engine.
        """
        environment = Environment(loader=FileSystemLoader(template_path))
        content = environment.get_template(template).render(data)
        # Subject is on first line of template
        self.subject = content.splitlines()[0]
        self.body_text = "\n".join(content.splitlines()[1:])
        self.body_html = markdown(self.body_text)

    def save(self, path):
        with open(path, "w") as file_handler:
            json.dump(
                {
                    "subject": self.subject,
                    "body_text": self.body_text,
                    "body_html": self.body_html,
                    "sender": self.sender,
                    "sender_name": self.sender_name,
                    "recipient": self.recipient,
                    "recipient_name": self.recipient_name,
                    "date": self.date,
                },
                file_handler,
            )

    def load(self, path):
        try:
            with open(path) as file_handler:
                data = json.load(file_handler)
            self.subject = data["subject"]
            self.body_text = data["body_text"]
            self.body_html = data["body_html"]
            self.sender = data["sender"]
            self.sender_name = data["sender_name"]
            self.recipient = data["recipient"]
            self.recipient_name = data["recipient_name"]
            self.date = data["date"]
        except:
            pass
        return self

PBEM - Chess - GAME {{app.game_id}}

Votre adversaire a joué {{app.moves[-1]}} :

    {{app.text_board}}

{%if app.board.is_stalemate()%}
La partie est bloquée car plus aucun mouvement ne peut être fait alors que le joueur
n'est pas en échec; celle-ci est donc nulle (pat).
{%elif app.board.is_insufficient_material()%}
Il n'y a plus suffisamment de pièces sur l'échiquier pour pouvoir gagner la partie.
Celle-ci est donc nulle (pat).
{%endif%}

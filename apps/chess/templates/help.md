PBEM - Chess - Help

# De quoi s'agit-il ?

Chess est une application permettant de jouer aux échecs face à un autre adversaire
humain (pas d'Intelligence Artificielle). La partie se joue uniquement par e-mail et
donc pas en temps réel. Il n'est donc pas possible simplement de jouer en mode *Blitz*.

# Les commandes

Les commandes sont à écrire dans le corps du mail, une commande par ligne.

## HELP

Cette commande permet de recevoir cette aide.

## RULES

Cette commande permet de recevoir les règles du jeu.

## NEWGAME

Cette commande, suivie d'une adresse mail, permet de créer et débuter une nouvelle
partie. Un mail sera alors envoyé au premier joueur (celui qui a les blancs) pour jouer
son premier coup. L'identifiant unique de la partie est également précisé.

## GAME

Cette commande, suivie de l'identiifant de la partie, permet de signaler au juge que
c'est la partie sur laquelle les commandes suivantes agiront. Elle est obligatoire pour
jouer un coup.

## PLAY

Cette commande, suivie du coup à jouer, permet de jouer un coup dans une partie
spécifiée auparavant grâce à la commande GAME.

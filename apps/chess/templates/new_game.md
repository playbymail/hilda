PBEM - Chess - GAME {{app.game_id}}

Une nouvelle partie vient de débuter. Vous jouez les blancs.

Plateau :

    {{app.text_board}}

Afin de jouer votre tour, il vous suffit d'envoyer un email avec une commande PLAY
suivie du coup à jouer au format UCI ou SAN.

exemple :

    GAME {{app.game_id}}
    PLAY e2e4

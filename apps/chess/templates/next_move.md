PBEM - Chess - GAME {{app.game_id}}

Votre adversaire a joué {{app.moves[-1]}} :

    {{app.text_board}}

{%if app.board.is_check%}Vous êtes actuellement en échec !{%endif%}

Afin de jouer votre tour, il vous suffit d'envoyer un email avec une commande PLAY
suivie du coup à jouer au format UCI ou SAN.

exemple :

    GAME {{app.game_id}}
    PLAY e2e4

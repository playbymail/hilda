PBEM - Chess - GAME {{app.game_id}}

Votre adversaire a joué {{app.moves[-1]}} :

    {{app.text_board}}

Vous êtes donc échec et mat; vous avez perdu la partie.

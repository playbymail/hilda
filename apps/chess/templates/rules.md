PBEM - Chess - Règles

# Règles du jeu officielles

cf. [Reprises depuis Wikipedia](https://fr.wikipedia.org/wiki/R%C3%A8gles_du_jeu_d%27%C3%A9checs)

Le jeu d'échecs oppose deux joueurs possédant seize pièces chacun, respectivement
blanches et noires, sur un échiquier de 64 cases. Chacun leur tour, les joueurs en font
évoluer une selon ses déplacements propres. Pour parler des adversaires, on dit « les
Blancs » et « les Noirs ».

## L'échiquier

 * Les huit lignes de cases verticales sont appelées colonnes (c'est-à-dire toutes les
    cases qui ont une lettre en commun).
 * les huit lignes de cases horizontales sont appelées rangées (ou plus rarement
    traverses) (c'est-à-dire toutes les cases qui ont un chiffre en commun).
 * les lignes obliques à 45° sont appelées diagonales.

Au départ, les Blancs sont toujours sur les rangées « 1 » et « 2 » et les Noirs sur les
rangées « 8 » et « 7 ». Par convention, chaque joueur doit avoir à sa droite une case
blanche (h1 pour les Blancs et a8 pour les Noirs).

    {{self.text_board}}

Afin de permettre la notation des coups, les colonnes sont désignées par des lettres
minuscules, de « a » à « h » (la colonne « a » étant la plus à gauche pour les Blancs),
et les rangées par des chiffres, de 1 à 8 (la rangée « 1 » étant celle des pièces
blanches.). Chaque case est ainsi représentée par une combinaison colonne-rangée, par
exemple « e5 ». Les indications de colonnes et de rangées sont parfois omises sur
l'échiquier ou le diagramme, seuls les joueurs débutants en ayant réellement besoin.

Chaque pièce est sur une seule case, et chaque case ne peut être occupée que par une
seule pièce. Chaque joueur possède initialement un roi, une dame, deux fous, deux
cavaliers, deux tours et huit pions.

Les dames se font face sur la colonne « d ». Il est proposé aux débutants comme moyen
mnémotechnique de placer les dames sur la case centrale de leur couleur, la dame blanche
sur une case blanche et la dame noire sur une case noire.

## Déroulement du jeu

Jouer un coup consiste à effectuer un déplacement de l'une de ses pièces, accompagné
éventuellement de la capture d'une pièce adverse se trouvant sur la case d'arrivée de la
pièce jouée(sauf si le coup joué est une prise en passant). À l'exception du roque (voir
ci-dessous), un coup ne peut pas être constitué du mouvement de deux pièces du même camp
à la fois. Si l'on décide de déplacer sa pièce sur la case occupée par une pièce
adverse, on retire cette pièce adverse de l'échiquier : elle a été prise ; contrairement
au jeu de dames, aucune prise n'est obligatoire aux échecs (à l'exception des cas où les
seuls coups légaux pour parer un échec consistent à prendre la pièce adverse qui
administre cet échec).

Blancs et Noirs jouent à tour de rôle. Les Blancs jouent le premier coup de la partie.

On dit de celui qui doit jouer qu'il a le trait, et jouer est une obligation (on ne peut
pas « passer » son tour). Être obligé de jouer est parfois un handicap lorsque tous les
coups à disposition se révèlent mauvais, on parle alors de zugzwang. Si le joueur qui a
le trait est dans l'impossibilité d'exécuter un coup légal, la partie se termine (c'est
un pat ou un échec et mat, voir plus loin).

Le temps de réflexion alloué à chaque joueur, le mode d'attribution des Blancs, le
nombre de rondes dans un tournoi, etc., ne font pas partie des règles du jeu lui-même,
ce sont des règles d'organisation des rencontres (partie amicale, par correspondance,
tournoi, etc.).

### Déplacements

Les bords de l'échiquier sont infranchissables par les pièces.

Aucune pièce ne peut venir occuper une case déjà occupée par une pièce de son propre
camp. Si une pièce (amie ou ennemie) se trouve dans la trajectoire de déplacement d'une
pièce à longue portée (dame, tour, ou fou), celle-ci est obligée de stopper son
déplacement ou de prendre la pièce (si elle n'est pas de la même couleur qu'elle), dans
lequel cas, elle stoppe son déplacement sur la case de la pièce prise.

Quand une pièce est touchée, elle doit être jouée (« pièce touchée, pièce jouée »). Tant
que la pièce n’est pas relâchée, sa trajectoire peut être modifiée.

### Le Roi

Le roi se déplace d'une case dans n'importe quelle direction. Il est interdit à un
joueur de mettre son propre roi en échec. Si cela se produit entre débutants, on demande
au joueur de reprendre ce coup illégal. À une cadence de jeu de blitz ou entre deux
joueurs plus expérimentés, la sanction serait la défaite pour le camp ayant joué ce coup
illégal.

### Le roque

Le roque est le seul cas où, en un seul coup, on peut déplacer deux de ses propres
pièces à la fois (le roi et une tour), et avec un mode de déplacement inhabituel : le
roi se déplace sur sa rangée de deux cases vers sa tour, et la tour saute par-dessus son
roi pour venir se placer à côté de lui, sur le flanc opposé. Il est nécessaire de
veiller à respecter l'ordre de ces deux déplacements : le roi se déplace en premier,
puis la tour opère son mouvement en second. Ce double déplacement est soumis aux
conditions suivantes :

 * le roi et la tour concernés n'ont jamais été déplacés ;
 * il n'y a aucune pièce entre le roi et la tour concernés ;
 * le roi n'est pas en échec au moment du roque ;
 * aucune des cases traversées par le roi n'est sous la menace d'une pièce adverse (le
    roi ne doit pas se mettre en échec lors de son déplacement).

Deux roques sont possibles : le petit roque (le roi se retrouve colonne g, la tour
colonne f) et le grand roque (roi en c, tour en d).

### Les pièces à longue portée

La tour, le fou et la dame sont des pièces à longue portée, cela signifie qu'elles
peuvent se déplacer de plusieurs cases en un seul coup, en ligne droite, tant qu'elles
ne sont pas limitées par l'obstacle infranchissable que constitue toute autre pièce,
adverse ou non.

### Le cavalier

Le cavalier est la seule pièce « sauteuse » du jeu. Depuis sa case de départ, il
« saute » directement sur sa case d’arrivée, grâce à son déplacement singulier : il se
déplace d'une case dans une direction horizontale ou verticale (comme une tour) puis
d'une case dans une direction en diagonale (comme un fou) ; on dit alors qu'il se
déplace en « Y ». Une autre visualisation possible pour décrire son déplacement est de
dire qu'il se déplace de deux cases devant lui (horizontalement ou verticalement) puis
qu'il va sur une case perpendiculaire ; on dit alors qu'il se déplace en « L » ou en
« T ».

Étant donné qu'il « bondit » directement sur sa case d'arrivée en se « faufilant » entre
les autres pièces (alliées ou adverses), la trajectoire du cavalier ne peut pas être
interceptée comme avec les autres pièces du jeu.

Le cavalier ne se déplace pas sur sa propre rangée, colonne ou sur ses diagonales. Les 8
cases d'arrivée possibles du cavalier forment une rosace (voir diagramme), ce qui lui
donne 8 possibilités de mouvement, dans le meilleur des cas.

### Le pion

Le pion se déplace droit devant lui (vers la 8e rangée pour les Blancs et vers la 1re
rangée pour les Noirs), d'une seule case à chaque coup et sans jamais pouvoir reculer.

Lors de son premier déplacement (alors qu'il est sur sa case initiale), un pion peut
avancer, au choix, d'une ou de deux cases en un seul coup (par exemple pour le pion
« e » des Blancs, e2-e3 ou e2-e4). Par contre, il est interdit de déplacer en même temps
deux pions différents d'une seule case. Dans les deux cas, la case d'arrivée doit être
libre de toute pièce amie ou ennemie ; si le pion se déplace de deux cases, aucune pièce
ne doit être sur son chemin.

Le pion ne capture pas les pièces adverses de la même façon que les autres pièces de son
camp. Il ne peut capturer une pièce adverse que si elle se trouve à une case en
diagonale de lui dans son sens de déplacement, il ne peut pas capturer de pièce qui se
trouverait devant lui, ne pouvant avancer que si la case devant lui est inoccupée.

### La promotion du pion

Quand le pion arrive sur la dernière rangée, il doit se transformer en une pièce de son
camp de valeur supérieure, au choix du joueur : dame, tour, fou ou cavalier. On dit
alors qu'il est « promu ». Le plus souvent, la promotion en dame est choisie, ce qui
permet ainsi d'avoir jusqu'à neuf dames si tous les pions vont à promotion, mais un
autre choix peut parfois se révéler plus judicieux, selon la position de la partie.

### La pièce clouée

Une pièce est dite « clouée » lorsque son déplacement exposerait directement le roi de
son camp à un échec. Il est donc interdit de déplacer une pièce clouée.

### Prise ou capture

Prendre une pièce adverse n'est pas obligatoire (sauf si c'est la seule façon de parer
un échec, ou plus simplement le seul coup jouable - mais alors, ce n'est pas la prise
qui est obligatoire, mais le coup ; on dira que la prise est forcée).

Une prise s'effectue en retirant la pièce prise du jeu et en mettant la pièce preneuse à
sa place, sauf dans la prise en passant. À part le roi (voir plus loin), aucune pièce
n'est par nature à l'abri d'une prise par l'adversaire, un simple pion est capable de
capturer la dame adverse. Le roi peut prendre n'importe quelle pièce non protégée qui se
trouve dans les huit cases qui l'entourent. Si cette pièce le met en échec et qu'aucune
pièce adverse ne la défend, capturer la pièce qui le met en échec peut être pour le roi
une manière de jouer ; il peut aussi fuir.

Toutes les pièces, sauf les pions, prennent comme elles se déplacent : l'obstacle sur le
trajet constitué par une pièce adverse est accessible avec la prise de cette pièce
adverse. Le roque est le seul cas de déplacement qui ne peut s'accompagner d'une prise.
Ce n'est pas une règle, c'est la conséquence naturelle des conditions liées à ce coup.

Les pions ont un mode de prise particulier : ils prennent en avançant d'une case en
diagonale.

### Prise en passant

La possibilité donnée au pion d'avancer de deux cases lors de son premier déplacement
lui permet d'éviter l'affrontement qui aurait eu lieu s'il n'avait avancé que d'une
case. Pour limiter ce désavantage qui pénalise l'audace du pion avancé adverse, ce
dernier a la possibilité de prendre comme si le coup de début n'avait été que d'une
case. Cette prise en passant ne peut se faire qu'en réponse immédiate à l'avance double.
C'est le seul cas où une pièce n'est pas capturée sur la case d'arrivée de la pièce qui
capture.

### Pièce touchée et adoubement

Dans le cas où une pièce est mal placée sur l'échiquier (entre deux cases par exemple)
et gêne le joueur, celui-ci peut, sur son temps de jeu, annoncer « j'adoube » — ou en
anglais « I adjust » — pour replacer la pièce au centre de sa case normale sans être
contraint de la jouer. Le jeu reprend ensuite normalement.

Dans les autres cas, le joueur qui touche une pièce est obligé de jouer celle-ci, ou de
la prendre s'il s'agit d'une pièce adverse, dans la mesure où c'est possible en fonction
des règles du jeu. S'il n'y a aucun coup légal possible avec la pièce touchée, le joueur
est libre de jouer avec n'importe quelle autre pièce. C'est la règle de la pièce
touchée.

Une fois la pièce lâchée sur la case finale, et pour autant qu'il s'agisse d'un coup
légal, le joueur ne peut plus revenir en arrière et reprendre ce coup. S'il ne s'agit
pas d'un coup légal, un autre coup doit être joué, tout en respectant la règle de pièce
touchée.

## Le gain de la partie

Quand un mouvement mène à une position qui menace le roi adverse de prise au prochain
coup, le joueur annonce parfois « échec au roi » ou plus simplement « échec ». L'annonce
vocale de l’« échec » au roi n'est pas obligatoire.

Le joueur concerné doit alors impérativement faire disparaître cette menace au coup
suivant, en utilisant l'une des trois possibilités suivantes :

 * déplacer son roi sur une case libre (non menacée) ;
 * capturer la pièce ennemie qui fait échec (ce type de parade est impossible si le roi
    attaqué est menacé par deux pièces simultanément, ou par une pièce ennemie protégée
    par une autre pièce) ;
 * interposer une pièce amie entre son roi et la pièce ennemie donnant échec, afin de
    faire obstacle à la menace de prise (ce type de parade est impossible contre un
    cavalier, un pion ou un échec double).

Un joueur n'a pas le droit de laisser son roi « en échec » ou de le mettre dans une
situation où il serait mis en échec.

Si le joueur dont le roi est mis en échec n'a pas de solution pour parer la menace, il
est alors « échec et mat » et perd la partie.

En compétition, les parties sont rarement jouées jusqu'au mat ; lorsqu'un joueur est en
infériorité au point que sa défaite est inévitable, il « abandonne » généralement la
partie. En général, jouer une position perdue d'avance jusqu'au mat (à part si celui-ci
est particulièrement beau) est considéré comme irrespectueux pour son adversaire (on lui
fait perdre son temps en espérant qu'il fasse une erreur).

## Parties nulles

La partie est dite « nulle », c'est-à-dire sans vainqueur, si l'une de ces conditions
survient :

 * le joueur qui a le trait n'est pas en échec, mais n'a aucun mouvement autorisé
    possible (c'est ce qu'on appelle être pat) ;
 * il n'y a de possibilité pour aucun des deux camps de mettre échec et mat le camp
    adverse par manque de pièces (exemple : roi contre roi) ou parce qu'il n'existe
    aucune suite de coups qui puisse conduire à un mat ;
 * les 50 derniers coups ont été joués par chaque joueur sans mouvement de pion ni prise
    de pièce ;
 * par répétition de la même position sur l'échiquier trois fois, consécutives ou non
    (deux positions sont identiques si le trait est le même et si les possibilités de
    prise en passant et de roque sont les mêmes) ;
 * par accord entre les deux joueurs ;
 * si un joueur perd au temps et que son adversaire n'a pas le matériel suffisant pour
    mater son adversaire, quelle que soit la suite de coups choisie.


# Notation des parties

Pour noter les parties d'échecs, plusieurs notations ont été proposées, parmi lesquelles
la notation algébrique, qui est pratiquement la seule à être restée en usage de nos
jours ; elle est d'ailleurs obligatoire lors des parties officielles. Il existe deux
variantes de notation algébrique, une « complète » dans laquelle sont notées les
positions initiales et finales des pièces, et une « abrégée » dans laquelle, sauf cas
exceptionnels, seules les positions finales des pièces sont indiquées.

Il existe encore d'autres types de notations, comme la notation descriptive (désuète de
nos jours, voir plus bas) et celles utilisées pour les ordinateurs ou les programmes
d'échecs.

## Notation algébrique

### Notation algébrique complète

Pour noter le coup d'un joueur dans la notation algébrique, on indique la nature de la
pièce (par son code ou rien pour le pion), les coordonnées de la case de départ, le
déplacement (« - ») ou la prise (« x ») et les coordonnées de la case d'arrivée.

Exemples :

 * e2-e4 : le pion en e2 au départ, se déplace en e4 ;
 * Cf6xe4  : le cavalier en f6 au départ, va sur la case e4 en prenant quelque chose non
    précisé par cette notation.

Le numéro d'ordre des coups est donné pour les Blancs et n'est pas répété pour les Noirs
dont le coup s'inscrit à la suite après une seule espace :

Ce qui pourrait donner, pour un début de partie :

 1. e2-e4 Cg8-f6
 2. d2-d4 Cf6xe4
 3. etc.

Dans ce système, des informations sont superflues, par exemple le code des pièces,
souvent la case de départ, la prise, etc.

### Notation algébrique abrégée

La notation abrégée, la plus répandue actuellement, omet la case de départ et le tiret
du déplacement. Le début ci-dessus devient :

 1. e4 Cf6
 2. d4 Cxe4
 3. etc.

Le système est en défaut quand deux pièces identiques peuvent aboutir sur la case notée.
Par exemple, avec deux tours, une en e1, l'autre en a1, si on écrit Td1, on ne sait pas
laquelle a joué. Il faut donc ajouter un élément de la case de départ permettant de
lever le doute : Ted1 si c'est la Te1 qui a joué, et s'il subsiste un doute en précisant
la colonne, on précise la rangée : par exemple, avec une tour en d1 et une tour en d3,
on indique T1d2 ou T3d2.

Pour la prise par les pions, on indique toujours la colonne de départ. Exemple :

 2. exd5 etc.

La prise en passant s'écrit e.p. (dans le diagramme plus haut, on écrirait : cxd3 e.p.,
bien que ce ne soit pas ambigu).

L'échec est indiqué par un « + » à la fin du coup, l'échec et mat par un « # ». Le pat,
l'abandon, la nullité (quelle qu'en soit la raison) n'ont pas de symbole, et doivent
être notés en clair. La proposition de partie nulle par un joueur doit également
figurer. On la note par un « (=) ».

Le petit roque est noté « O-O » et le grand roque « O-O-O ».

### Notation descriptive

La notation descriptive est une notation apparue vers les XIe – XIIe siècles et utilisée
jusqu’à la fin du XXe siècle dans les pays anglo-saxons et hispaniques, mais qui a
depuis été délaissée au profit de la notation algébrique, plus simple à comprendre et à
écrire. Elle est encore lisible aujourd'hui en consultant d'anciennes publications sur
les échecs (livres, revues, etc.).

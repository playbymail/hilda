# encoding: utf-8
"""
Simple chess game.
"""

from random import shuffle
import os

from dotenv import load_dotenv

from application import Application
import chess
import tools


EMAIL = r"[a-zA-Z0-9_.-]+@[a-zA-Z0-9_.-]+"
GAME_ID = r"[A-Z0-9]+"
PLAY = r"[^\s]+"


class App(Application):
    load_dotenv()
    def __init__(self):
        super().__init__(
            name="chess",
            path=os.path.abspath(os.path.dirname(__file__)),
            sender=os.getenv("SENDER"),
            sender_name=os.getenv("SENDER_NAME"),
        )
        self.commands.update(
            {
                "new_game": fr"^NEWGAME\s+(?P<address>{EMAIL})$",
                "load": fr"^GAME\s+(?P<game_id>{GAME_ID})$",
                "play": fr"^PLAY\s+(?P<move>{PLAY})$",
                "resign": fr"^RESIGN$",
            }
        )
        self.game_id = None
        self.board = None
        self.error = None
        self.players = []
        self.moves = []

    @property
    def text_board(self):
        return str(self.board).replace("\n", "\n\t")

    def serialize(self):
        return {
            "game_id": self.game_id,
            "players": self.players,
            "board": self.board.fen(),
            "moves": self.moves,
        }

    def unserialize(self, data):
        self.game_id = data["game_id"]
        self.players = data["players"]
        self.board = chess.Board(fen=data["board"])
        self.moves = data["moves"]

    def process_subject(self):
        if "GAME" in self.message.subject:
            game_id = self.message.subject.split("GAME")[1].split()[0]
            self.load(game_id)

    def help(self, subject=None):
        self.send_message(self.message.sender, "help.md")

    def rules(self):
        game_id = None
        if self.game_id is not None:
            game_id = self.game_id
            self.game_id = None
        self.board = chess.Board()
        self.board.reset()
        self.send_message(self.message.sender, "rules.md")
        if game_id is not None:
            self.load(game_id)

    def new_game(self, address):
        self.logger.info(f"new game: {address}")
        self.game_id = tools.pick_id(4, self.app_path)
        self.board = chess.Board()
        self.board.reset()
        self.players = [self.message.sender, address]
        # Game starts, let's choose colors
        shuffle(self.players)
        self.save()
        # Send mail to first/white player
        self.send_message(self.players[self.board.turn], "new_game.md")

    def play(self, move):
        # No game loaded
        if self.game_id is None:
            self.logger.warning("No game loaded")
            self.error = (
                "Il semble qu'aucune partie n'ait été chargée. Avez-vous bien spécifié"
                " un identifiant via la commande GAME ? Si oui, peut-être vous"
                " êtes-vous trompé ? Merci de vérifier et de ré-essayer."
            )
            self.send_message(self.message.sender, "error.md")
            return
        # Am I part of this game ?
        if self.message.sender not in self.players:
            self.logger.warning("Not your game!")
            self.error = (
                "Vous ne faites pas partie de ce jeu. Merci de vérifier que"
                " l'identifiant de la partie est le bon et que l'adresse mail utilisée"
                " est également correcte."
            )
            self.send_message(self.message.sender, "error.md")
            return
        # Is this my turn ?
        if self.players[self.board.turn] != self.message.sender:
            self.logger.warning("Not your turn!")
            self.error = (
                "Ce n'est pas à votre tour de jouer. Merci d'attendre un email vous"
                " indiquant le coup de votre adversaire."
            )
            self.send_message(self.message.sender, "error.md")
            return
        # Legal move ?
        try:
            uci_move = self.board.parse_san(move)
        except ValueError:
            self.logger.warning("Invalid move")
            self.error = (
                "Votre coup semble invalide. Êtes-vous certain qu'il s'agit d'un coup"
                " légal ou que vous l'avez écrit correctement ? En cas de doute, vous"
                " pouvez créer un bug en envoyant un mail à l'adresse *bug*."
            )
            self.send_message(self.message.sender, "error.md")
            return
        # Everything is ok, let's play
        self.moves.append(uci_move.uci())
        self.board.push(uci_move)
        self.save()
        # Send email to opponent
        if self.board.is_checkmate():
            self.send_message(self.players[self.board.turn], "lost_game.md")
            self.send_message(self.players[not self.board.turn], "win_game.md")
            # Delete game
            self.delete(self.game_id)
        elif self.board.is_stalemate() or self.board.is_insufficient_material():
            self.send_message(self.players[self.board.turn], "pat_game.md")
            self.send_message(self.players[not self.board.turn], "pat_game.md")
            # Delete game
            self.delete(self.game_id)
        else:
            self.send_message(self.players[self.board.turn], "next_move.md")

    def resign(self):
        # No game loaded
        if self.game_id is None:
            self.logger.warning("No game loaded")
            self.error = (
                "Il semble qu'aucune partie n'ait été chargée. Avez-vous bien spécifié"
                " un identifiant via la commande GAME ? Si oui, peut-être vous"
                " êtes-vous trompé ? Merci de vérifier et de ré-essayer."
            )
            self.send_message(self.message.sender, "error.md")
            return
        # Sending emails
        self.send_message(self.players[0], "resign.md")
        self.send_message(self.players[1], "resign.md")
        # Delete game
        self.delete(self.game_id)
